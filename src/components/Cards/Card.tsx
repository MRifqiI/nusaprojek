import React, { useState } from 'react'
import { Card } from 'antd'

function MaterialCard({ material }) {
  return (
    <div className="flex gap-[14px]">
      {material.map((item, index) => (
        // eslint-disable-next-line no-use-before-define, react/no-array-index-key
        <CheckableMaterialCard key={index} value={item.value} img={item.img} />
      ))}
    </div>
  )
}

function BudgetCard({ budget }) {
  return (
    <div className="flex gap-[14px]">
      {budget.map((item, index) => (
        // eslint-disable-next-line no-use-before-define, react/no-array-index-key
        <CheckableBudgetCard key={index} value={item.value} />
      ))}
    </div>
  )
}

function CheckableMaterialCard({ value, img }) {
  const [isChecked, setIsChecked] = useState(false)

  const handleCardClick = () => {
    setIsChecked(!isChecked)
  }

  return (
    <Card
      className={`rounded-xl border w-[147px] h-[147px] hover:border-2 hover:border-red-700 ${
        isChecked ? 'border-red-700' : ''
      }`}
      onClick={handleCardClick}
    >
      {img && (
        // eslint-disable-next-line jsx-a11y/img-redundant-alt
        <img
          src={img}
          alt={`Image for ${value}`}
          className="w-[100px] h-[50px] mx-auto mb-10"
        />
      )}
      <Card.Meta className="text-center" title={value} />
      {isChecked && (
        <div className="absolute top-[-15px] right-[53px]">
          <img src="./static/images/icons/icon-checklist.svg" alt="Checkmark" />
        </div>
      )}
    </Card>
  )
}

function CheckableBudgetCard({ value }) {
  const [isChecked, setIsChecked] = useState(false)

  const handleCardClick = () => {
    setIsChecked(!isChecked)
  }

  return (
    <Card
      className={`rounded-xl border w-[147px] h-[100px] hover:border-2 hover:border-red-700 ${
        isChecked ? 'border-red-700' : ''
      }`}
      onClick={handleCardClick}
    >
      <div className="h-[50px] flex justify-center items-center">{value}</div>
      {isChecked && (
        <div className="absolute top-[-15px] right-[53px]">
          <img src="./static/images/icons/icon-checklist.svg" alt="Checkmark" />
        </div>
      )}
    </Card>
  )
}

export { MaterialCard, BudgetCard }
